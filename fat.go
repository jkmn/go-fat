package fat

/*
Regarding type naming conventions:
I wanted most types to be private, but I didn't want to get them confused with variables, so I intentionally made the type names irritating and difficult.
*/

/*
Regarding the lack of segregation between code dealing with the different FAT types:
I originally tried to keep the FAT types very segregated in code. Because of the significant amount of overlap, I ended up with a lot of repitition of code. Consequently, I merged the structs and all the code became much simpler.
*/

import (
	"github.com/jackmanlabs/errors"
	"os"
)

// Standard error for nil file entry.
var (
	ErrNoFileEntry = errors.New("There is no file entry at the address specified.")
	ErrPath        = errors.New("An invalid path was provided. Please use a *NIX-style path beginning from root ('/').")
)

// FAT represents a FAT file system.
type FAT struct {
	tBPB
	devPath string
}

// Device is a subset of the interface provided by os.File.
// In part, this interface was created to remove the requirement to include package os in this one.
type Device interface {
	Close() error
	Read(b []byte) (n int, err error)
	ReadAt(b []byte, off int64) (n int, err error)
	Seek(offset int64, whence int) (ret int64, err error)
}

func (f *FAT) openDevice() (Device, error) {
	file, err := os.Open(f.devPath)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return file, nil
}

/*
NewFAT reads a file and returns an object capable of reading and writing data to a FAT file system contained in the file.
*/
func NewFAT(devPath string) (*FAT, error) {
	fat := new(FAT)
	fat.devPath = devPath

	dev, err := fat.openDevice()
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer dev.Close()

	bpb, err := parseBPB(dev)
	if err != nil {
		return nil, errors.Stack(err)
	}
	fat.tBPB = bpb

	return fat, nil
}

func (f *FAT) getTableEntry(ci int) (int, error) {
	switch f.FATType() {
	case 12:
		return f.getTableEntry12(ci)
	case 16:
		return 0, errors.New("Not yet implemented.")
	case 32:
		return 0, errors.New("Not yet implemented.")
	}

	return 0, errors.New("Unreachable code reached.")
}

/*
 getEntry accepts a cluster index of the FAT and returns the value
for that index.
*/
func (f *FAT) getTableEntry12(ci int) (int, error) {

	/*
	   Due to integer math, the byte offset will always point to the first byte we need first.
	   If the cluster index is even, we use the entire first byte and half the next byte.
	   If the cluster index is odd, we use the latter half of the first byte and the entire second byte.
	*/

	var (
		byteOffset int
		byteBase   int
		z          []byte = make([]byte, 2)
		entry      int    = 0
	)

	byteOffset = ci * 3 / 2
	byteBase = f.SectorsReserved * f.BytesXSector

	dev, err := f.openDevice()
	if err != nil {
		return 0, errors.Stack(err)
	}
	defer dev.Close()

	// Get two bytes.
	_, err = dev.ReadAt(z, int64(byteBase+byteOffset))
	if err != nil {
		return 0, errors.Stack(err)
	}

	entry = bToInt(z) // conveniently reorders the bytes

	if (ci & 0x01) == 0 { // if even
		entry = entry & 0x0FFF
	} else {
		entry = entry >> 4
	}

	return entry, nil
}

// GetFAT returns the FAT of this system; mostly for debugging.
func (f *FAT) GetFAT() ([]int, error) {
	clusters := f.clustersData()

	var (
		table []int = make([]int, clusters)
		err   error
	)
	for i := 0; i < clusters; i++ {
		table[i], err = f.getTableEntry(i)
		if err != nil {
			return table, errors.Stack(err)
		}
	}

	return table, nil
}

func (f *FAT) getCluster(ci int) ([]byte, error) {
	bytesPerCluster := f.BytesXSector * int(f.SectorsXCluster)
	byteOffset := ci * bytesPerCluster

	data := make([]byte, bytesPerCluster)

	dev, err := f.openDevice()
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer dev.Close()

	offset, err := dev.Seek(byteOffset, 0)
	if err != nil {
		return nil, errors.Stack(err)
	}

	_, err = dev.Read(data)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return data
}
