package fat

/*
Regarding the segregation of BPB and FAT code:
The BPB code deals with the parsing of BPB data and the mathematical derivations of that data. FAT code deals with the extraction, modification, and insertion of data. FAT code depends on BPB code.
You can also think of BPB code dealing with the BPB area of the file system while the FAT code deals with the data area(s).
*/

import (
	"github.com/jackmanlabs/errors"
)

type tBPB struct {
	/*
	   Please Note:
	   I apologize for breaking strict convention, but I felt it was important to emphasize utility of the fields here by making non-trustworthy fields lowercase (private) and trustworthy fields uppercase (public). This is to remind the programmer that any calculation depending on the private fields cannot be relied upon when switching between FAT12 and FAT32 file systems. Use the uppercase fields or methods for calculations!
	*/

	// BIOS Parameter Block fields:
	BootJump        []byte // Off: 0  Len: 3
	OEMID           string // Off: 3  Len: 8
	BytesXSector    int    // Off: 11 Len: 2
	SectorsXCluster byte   // Off: 13 Len: 1
	SectorsReserved int    // Off: 14 Len: 2
	TableCount      int    // Off: 16 Len: 1 // The number of FATs.
	RootEntries     int    // Off: 17 Len: 2 // The number of root entries.
	sectorsTotal16  int    // Off: 19 Len: 2
	MediaType       byte   // Off: 21 Len: 1
	tableSize16     int    // Off: 22 Len: 2 // Size of the FAT in sectors.
	SectorsXTrack   int    // Off: 24 Len: 2
	Heads           int    // Off: 26 Len: 2
	SectorsHidden   int    // Off: 28 Len: 4
	sectorsTotal32  int    // Off: 32 Len: 4

	// Shared Extended BIOS Parameter Block fields:
	// See implementation for locations.
	DriveNumber byte
	NTFlags     byte
	Signature   byte
	VolumeID    []byte
	VolumeLabel string
	SystemLabel string
	BootCode    []byte
	Bootable    []byte

	// FAT32 Extended BIOS Parameter Block fields:
	tableSize32      int    // Off: 36 Len: 4
	Flags            []byte // Off: 40 Len: 2
	FATVersion       []byte // Off: 42 Len: 2
	RootCluster      int    // Off: 44 Len: 4
	FSInfoSector     int    // Off: 48 Len: 2
	BackupBootSector int    // Off: 50 Len: 2
}

func parseBPB(source Device) (tBPB, error) {

	var (
		b    tBPB
		data []byte = make([]byte, 512)
	)

	if _, err := source.ReadAt(data, 0); err != nil {
		return b, errors.Stack(err)
	}

	b.BootJump = data[0:3]
	b.OEMID = string(data[3:11])
	b.BytesXSector = bToInt(data[11:13])
	b.SectorsXCluster = data[13]
	b.SectorsReserved = bToInt(data[14:16])
	b.TableCount = bToInt(data[16:17])
	b.RootEntries = bToInt(data[17:19])
	b.sectorsTotal16 = bToInt(data[19:21])
	b.MediaType = data[21]
	b.tableSize16 = bToInt(data[22:24])
	b.SectorsXTrack = bToInt(data[24:26])
	b.Heads = bToInt(data[26:28])
	b.SectorsHidden = bToInt(data[28:32])
	b.sectorsTotal32 = bToInt(data[32:36])

	if b.tableSize16 == 0 { // FAT12/FAT16 fields only.
		b.DriveNumber = data[36]            // Off: 36  Len: 1
		b.NTFlags = data[37]                // Off: 37  Len: 1
		b.Signature = data[38]              // Off: 38  Len: 1
		b.VolumeID = data[39:43]            // Off: 39  Len: 4
		b.VolumeLabel = string(data[43:54]) // Off: 43  Len: 11
		b.SystemLabel = string(data[54:62]) // Off: 54  Len: 8
		b.BootCode = data[62:510]           // Off: 62  Len: 448
		b.Bootable = data[510:512]          // Off: 510 Len: 2
	} else { // Include FAT32 fields.
		b.tableSize32 = bToInt(data[36:40])
		b.Flags = data[40:42]
		b.FATVersion = data[42:44]
		b.RootCluster = bToInt(data[44:48])
		b.FSInfoSector = bToInt(data[48:50])
		b.BackupBootSector = bToInt(data[50:52])
		b.DriveNumber = data[64]            // Off: 64  Len: 1
		b.NTFlags = data[65]                // Off: 65  Len: 1
		b.Signature = data[66]              // Off: 66  Len: 1
		b.VolumeID = data[67:71]            // Off: 67  Len: 4
		b.VolumeLabel = string(data[71:82]) // Off: 71  Len: 11
		b.SystemLabel = string(data[82:90]) // Off: 82  Len: 8
		b.BootCode = data[90:510]           // Off: 90  Len: 420
		b.Bootable = data[510:512]          // Off: 510 Len: 2
	}

	return b, nil
}

func (b *tBPB) firstFATSector() int {
	return b.SectorsReserved
}

func (b *tBPB) firstDataSector() int {
	return b.SectorsReserved + b.TableCount*b.tableSize() + b.sectorsRoot()
}

func (b *tBPB) sectorsTotal() int {
	if b.sectorsTotal16 == 0 {
		return b.sectorsTotal32
	} else {
		return b.sectorsTotal16
	}
}

// This should always return zero for FAT32 file headers.
func (b *tBPB) sectorsRoot() int {
	// additional bytes added for rounding.
	bytes := b.RootEntries*32 + (b.BytesXSector - 1)
	sectors := bytes / b.BytesXSector
	return sectors
}

func (b *tBPB) tableSize() int {
	if b.tableSize16 == 0 {
		return b.tableSize32
	} else {
		return b.tableSize16
	}
}

// FATType returns an integer representing the type of FAT system.
func (b *tBPB) FATType() int {
	clusters := b.clustersData()
	if clusters < 4085 {
		return 12
	} else if clusters < 65525 {
		return 16
	} else {
		return 32
	}
}

// In a lot of documentation, this is named something more like 'clustersTotal', but I keep having to remind myself that clusters only exist in the data area. This name makes a lot more sense to me.
func (b *tBPB) clustersData() int {
	return b.sectorsData() / int(b.SectorsXCluster)
}

func (b *tBPB) sectorsData() int {
	nondata := b.SectorsReserved + b.TableCount*b.tableSize() + b.sectorsRoot()
	return b.sectorsTotal() - nondata
}
