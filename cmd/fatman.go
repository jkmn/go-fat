package main

import (
	"flag"
	"fmt"
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/errors"
	fat "go-fat"
	"log"
	"os"
)

var (
	devPath = flag.String("dev", "", "Path to the device/image containing the FAT filesystem. Required for all operations.")
	fs      *fat.FAT
)

func main() {
	flag.Parse()

	log.Printf("Using device file: %s", *devPath)

	if *devPath == "" {
		log.Println("ERROR: No input file specified.")
		flag.Usage()
		os.Exit(1)
	}

	var err error
	fs, err = fat.NewFAT(*devPath)
	if err != nil {
		log.Fatal("Failure creating new FAT description:", err)
	}

	args := flag.Args()

	var operation string
	if len(args) >= 1 {
		operation = args[0]
	}

	switch operation {
	case "ls":
		var path string
		if len(args) >= 2 {
			path = args[1]
		} else {
			path = "/"
		}

		err = ls(path)
	case "mkdir":
	case "cpin":
	case "cpout":
	case "info":
		var path string
		if len(args) >= 2 {
			path = args[1]
		} else {
			path = "/"
		}

		err = info(path)
	default:
		err = errors.New("No command provided or command not recognized.")
	}

	if err != nil {
		log.Fatal(err)
	}
}

func ls(path string) error {
	format := "02-01-06 15:04 MST"
	formatd := "02-01-06"

	files, err := fs.ListFiles(path)
	if err != nil {
		return errors.Stack(err)
	}

	fmt.Printf("File listing for '%s':\n", path)
	fmt.Printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		"NAME    .EXT",
		"Size",
		"RHSVDA",
		"Creation Date     ",
		"Modfied Date      ",
		"Access Date",
		"Long Name",
	)
	for _, f := range files {
		name := fmt.Sprintf("%s.%s", f.Name, f.Ext)

		fmt.Printf("%s\t%d\t%s\t%s\t%s\t%s\t%s\n",
			name,
			f.Size(),
			f.Attr(),
			f.Creation().Format(format),
			f.Modified().Format(format),
			f.Accessed().Format(formatd),
			f.NameLong,
		)
	}

	return nil
}

func info(path string) error {

	file, err := fs.GetFile(path)
	if err != nil {
		return errors.Stack(err)
	}

	jlog.Log(file)

	return nil
}

func cpin(src, dst string) error { return nil }

func cpout(src, dst string) error { return nil }
