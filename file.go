package fat

import (
	"bytes"
	"github.com/jackmanlabs/errors"
	"sort"
	"strings"
	"time"
)

/*
FileInfo describes a file in a FAT12/16/32 file system.
A zero-value FileInfo is not valid.
Only a FileInfo provided via an insert, a listing, or retrieval is valid.
*/
type FileInfo struct {
	Name, Ext  string        // Off: 00 Len: 11 // 8+3
	ReadOnly   bool          // Off: 11 Len: 1 Mask: 0x01
	Hidden     bool          // Off: 11 Len: 1 Mask: 0x02
	System     bool          // Off: 11 Len: 1 Mask: 0x04
	VolumeID   bool          // Off: 11 Len: 1 Mask: 0x08
	Directory  bool          // Off: 11 Len: 1 Mask: 0x10
	Archive    bool          // Off: 11 Len: 1 Mask: 0x20
	ntReserved byte          // Off: 12 Len: 1
	createTime time.Duration // Off: 13 Len: 1 // x/100 sec.
	creation   time.Time     // Off: 14 Len: 4 // Date + Time
	accessed   time.Time     // Off: 18 Len: 2 // Date only.
	modified   time.Time     // Off: 22 Len: 4 // Date + Time
	cluster    int           // Off: 26 Len: 2 // Hi Bytes: 20:2
	size       int           // Off: 28 Len: 4

	longData tLongData
	NameLong string // Set if long data provides a name.
}

// Size is the getter for the size of the file in bytes.
func (f *FileInfo) Size() int {
	return f.size
}

// Creation is the getter for the creation date/time.
func (f *FileInfo) Creation() time.Time {
	return f.creation
}

// Modified is the getter for the modified date/time.
func (f *FileInfo) Modified() time.Time {
	return f.modified
}

// Accessed is the getter for the access date/time.
func (f *FileInfo) Accessed() time.Time {
	return f.accessed
}

type tLongData []tLongDatum

type tLongDatum struct {
	Order     byte   // Off: 0 Len: 1
	Payload   string // Scattered: (1:10),(14:12),(28:4)
	EntryType byte   // Off: 0 Len: 1
	Checksum  byte   // Off: 0 Len: 1
}

func (ld tLongData) Len() int           { return len(ld) }
func (ld tLongData) Swap(i, j int)      { ld[i], ld[j] = ld[j], ld[i] }
func (ld tLongData) Less(i, j int) bool { return ld[i].Order < ld[j].Order }

// This method assumes that a seek has been performed to the proper location on the device.
func parseFile(source Device) (*FileInfo, error) {
	fi := new(FileInfo)
	fi.longData = make(tLongData, 0)

	// By performing a recursive parsing, we avoid a lot of awkward buffering.
	err := parseFileRecursive(source, fi)
	if err == ErrNoFileEntry {
		return nil, err
	} else if err != nil {
		return nil, errors.Stack(err)
	}

	// Generate the long name from the long data.
	names := make([]string, len(fi.longData))
	sort.Sort(fi.longData)
	for i, ld := range fi.longData {
		names[i] = ld.Payload
	}
	fi.NameLong = strings.Join(names, "")
	fi.NameLong = strings.Trim(fi.NameLong, "\x00")

	return fi, nil
}

func parseFileRecursive(source Device, file *FileInfo) error {
	var z []byte = make([]byte, 32) // file chunks are 32 bytes long.

	if _, err := source.Read(z); err != nil {
		return errors.Stack(err)
	}

	if (z[0] == 0) || (z[0] == 0xE5) {
		return ErrNoFileEntry
	}

	attr := z[11]
	if attr == 0x0F {
		var ld tLongDatum

		name := new(bytes.Buffer)
		name.Write(z[1:11])
		name.Write(z[14:26])
		name.Write(z[28:32])

		ld.Payload = bToString2(name.Bytes())
		ld.Order = z[0]
		ld.Checksum = z[13]
		ld.EntryType = z[12]

		file.longData = append(file.longData, ld)

		parseFileRecursive(source, file)
	} else {
		file.Name = bToString1(z[0:8])
		file.Ext = bToString1(z[8:11])
		file.ReadOnly = (z[11] & 0x01) > 0
		file.Hidden = (z[11] & 0x02) > 0
		file.System = (z[11] & 0x04) > 0
		file.VolumeID = (z[11] & 0x08) > 0
		file.Directory = (z[11] & 0x10) > 0
		file.Archive = (z[11] & 0x020) > 0
		file.ntReserved = z[12]
		file.createTime = time.Duration(z[13]) * time.Second / 100
		file.creation = bToTime(z[14:18])
		file.modified = bToTime(z[22:26])
		file.accessed = bToDate(z[18:20])

		cluster0 := bToInt(z[26:28])
		cluster1 := bToInt(z[20:22])
		cluster1 = cluster1 << 16
		file.cluster = cluster0 | cluster1
		file.size = bToInt(z[28:32])

	}
	return nil
}

/*
Attr generates the abbreviated attribute string.
*/
func (f *FileInfo) Attr() string {
	var r []rune = []rune{' ', ' ', ' ', ' ', ' ', ' '}

	if f.ReadOnly {
		r[0] = 'R'
	}
	if f.Hidden {
		r[1] = 'H'
	}
	if f.System {
		r[2] = 'S'
	}
	if f.VolumeID {
		r[3] = 'V'
	}
	if f.Directory {
		r[4] = 'D'
	}
	if f.Archive {
		r[5] = 'A'
	}

	return string(r)
}

/*
GetFile retrieves a file with the given path name.
Paths should be provided in the typical *NIX way:
 * Include a preceeding forward slash to indicate the root directory
 * Each (sub-)directory is seperated by a forward slash.
 * The filename is appended after another forward slash.
 * Whitespace is not allowed unless it is a part of a file or directory name.
*/
func (f *FAT) GetFile(path string) (*FileInfo, error) {
	if path == "/" {
		return nil, errors.New("Cannot provide file info for the root directory.")
	}

	chunks := strings.Split(path, "/")

	files, err := f.rootFiles()
	if err != nil {
		return nil, errors.Stack(err)
	}

	var fi *FileInfo

	for _, chunk := range chunks {
		
		if fi != nil{
			if fi.Directory{
				files, err = f.directoryContents(fi)
			}
		}
		
		for _, file := range files {
			if chunk == file.Name || chunk == file.NameLong {
				fi = &file
			}
		}
	}

	return fi, nil
}
