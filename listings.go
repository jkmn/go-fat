package fat

import (
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/errors"
	"strings"
)

/*
ListFiles returns a FileInfo struct for each file in the specified directory.
The directory should be specified with a path in the typical *NIX way.
*/
func (this *FAT) directoryContents(fi FileInfo) ([]FileInfo, error) {
	cluster, err := this.getCluster(fi.cluster)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return nil, nil
}

func (f *FAT) rootFiles() ([]FileInfo, error) {

	if f.FATType() == 32 {
		return f.rootFiles32()
	} else {
		return f.rootFiles12()
	}
}

func (f *FAT) rootFiles12() ([]FileInfo, error) {
	var files []FileInfo = make([]FileInfo, 0)

	// The root entries should be right after the FATs.
	baseSector := f.SectorsReserved + f.TableCount*f.tableSize()
	byteOffset := int64(baseSector * f.BytesXSector)
	upperLimit := byteOffset + int64(f.RootEntries)*32

	dev, err := f.openDevice()
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer dev.Close()

	offset, err := dev.Seek(byteOffset, 0)
	if err != nil {
		return files, errors.Stack(err)
	}

	/*
		At the beginning of the following loop,
		the pointer is positioned directly after
		the partition table, at the beginning of
		the entry listing.

		The upper limit is the lower limit plus
		the total allowable size of the root
		entry listing.
	*/

	for offset < upperLimit {
		file, err := parseFile(dev)
		if err == ErrNoFileEntry {
			// do not append an empty file.
			// 'continue' not used so offset can be calculated below.
		} else if err != nil {
			return files, errors.Stack(err)
		} else {

			// file parsing was successful.
			files = append(files, *file)
		}

		// Determine current offset from file for loop.
		// This does not modify the offset in any way.
		offset, err = dev.Seek(0, 1)
		if err != nil {
			return files, errors.Stack(err)
		}
	}
	return files, nil
}

func (f *FAT) rootFiles32() ([]FileInfo, error) {
	return nil, errors.New("Not yet implemented.")
}
