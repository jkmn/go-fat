package fat

import (
	"time"
)

func bToString2(b []byte) string {
	var runes []rune = make([]rune, 0)
	for len(b) > 0 {
		z := bToInt(b[0:2])
		if z == 0 {
			break // reached end of string.
		}
		runes = append(runes, rune(z))
		b = b[2:]
	}
	return string(runes)
}
func bToString1(b []byte) string {
	var runes []rune = make([]rune, 0)
	for _, r := range b {
		if r == 0 {
			break // end of string
		}
		runes = append(runes, rune(r))
	}
	return string(runes)
}

// Converts a little-endian string of bytes to an integer.
func bToInt(data []byte) int {
	var output int = 0
	for i := len(data) - 1; i >= 0; i-- {
		output = output << 8
		output = output | int(data[i])
	}
	return output
}

func bToTime(b []byte) time.Time {
	z := bToInt(b)

	sec := z & 0x1F
	min := (z >> 5) & 0x3F
	hr := (z >> 11) & 0x1F

	z >>= 16

	day := z & 0x1F
	mo := (z >> 5) & 0x0F
	yr := (z >> 9) & 0x7F
	yr += 1980

	loc, _ := time.LoadLocation("Local")

	return time.Date(yr, time.Month(mo), day, hr, min, sec, 0, loc)
}

func bToDate(b []byte) time.Time {
	z := bToInt(b)

	mo := (z >> 5) & 0x0F
	day := z & 0x1F
	yr := (z >> 9) & 0x7F
	yr += 1980

	loc, _ := time.LoadLocation("Local")

	return time.Date(yr, time.Month(mo), day, 0, 0, 0, 0, loc)
}
