package fat

import (
//	"bytes"
//  "io"
)

// ChainReader is a reader that will traverse and provide data from a FAT linked list (cluster chain).
type ChainReader struct {
	firstCluster   int
	rOffset        int
	fat            *FAT
	currentCluster int // Current cluster in the chain.
	currentByte    int // Current byte in the cluster.
}

func NewFileReader(fat *FAT, fi *FileInfo) *ChainReader {
	cr := &ChainReader{
		firstCluster:   fi.cluster,
		currentCluster: fi.cluster,
		currentByte:    0,
		fat:            fat,
	}

	return cr
}

//func (cr *ChainReader) Read(p []byte) (n int, err error) {
//
//
//		max := len(p)
//		count := 0 // current number of bytes read.
//		buf := new(bytes.Buffer)
//
//		// cluster size in bytes
//		cSize := int(cr.fat.SectorsXCluster) * cr.fat.BytesXSector
//
//		// get the next cluster.
//
//	return
//}
